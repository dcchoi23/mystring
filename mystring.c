#include "mystring.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/*
 *   Implement the following functions: 
 * 
 *   You are NOT allowed to use any standard string functions such as 
 *   strlen, strcpy or strcmp or any other string function.
*/

/*
 *  mystrlen() calculates the length of the string s, 
 *  not including the terminating character '\0'.
 *  Returns: length of s.
*/
int mystrlen (const char *s) 
{
	assert(s);
	int length = 0;
	for(int i = 0; s[i] != '\0'; i++){
		length++;
	}
	return length;
}

/*
 *  mystrcpy()  copies the string pointed to by src (including the terminating 
 *  character '\0') to the array pointed to by dst.
 *  Returns: a  pointer to the destination string dst.
 */
char  *mystrcpy (char *dst, const char *src)
{
	assert(src);
	int j = 0;
  	for(int i = 0; i < mystrlen(src); i++){
  		dst[i] = src[i];
  		j++;
  	}
  	dst[j] = '\0';  //NULL pointer at end of string needs to be added since it wasn't copied

	return dst;
}

/*
 * mystrcmp() compares two strings alphabetically
 * Returns: 
 * 	-1 if s1  < s2
 *  	0 if s1 == s2
 *  	1 if s1 > s2
 */
int mystrcmp(const char *s1, const char *s2)
{
	assert(s1);
	assert(s2);
	//Need to check if the string's first character is NULL then return 0
	if(s1[0] == '\0' && s2[0] == '\0'){ 
		return 0;
	}

	//If string s1's first character is NULL and s2's first character not null then s1 < s2 so return -1
	else if(s1[0] == '\0' && s2[0] != '\0'){
		return -1;
	}

	//If string s2's first character is NULL and s1's first character not null then s1 > s2 so return 1 
	else if(s1[0] != '\0' && s2[0] == '\0'){
		return 1;
	}

	else {
		//Run this for loop to check the individual characters
		for(int i = 0; s1[i] != '\0' && s2[i] != '\0'; i++){
			if(s1[i] != s2[i]){
				if(s1[i] < s2[i]){
					return -1;
				}
				else{
					return 1;
				}
			}
		}
	}
	
	return 0;
}

/*
 * mystrdup() creates a duplicate of the string pointed to by s. 
 * The space for the new string is obtained using malloc.  
 * If the new string can not be created, a null pointer is returned.
 * Returns:  a pointer to a new string (the duplicate) 
 	     or null If the new string could not be created for 
	     any reason such as insuffient memory. 
*/
char *mystrdup(const char *s1)
{

	assert(s1);
	int j = 0;
	
	char* newstring = NULL;
	//Allocate dynamic heap memory equal to the size of s1
	 newstring = (char* )malloc(mystrlen(s1)+1);
	
	//Check if memory has successfully been allocated 
	if(newstring == NULL){
		return NULL;
	}

	//Run a for loop to transfer each string character into the array pointed to by newstring 
	for(int i = 0; i < mystrlen(s1); i++){
			j++;
			newstring[i] = s1[i];
	}
	newstring[j] = '\0'; //Null pointer not copied yet so add null pointer to end of string 

	return newstring;
}

