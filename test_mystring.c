#include "mystring.h"
#include <assert.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/*
 * The following are simple tests to help you.
 * You should develop more extensive tests, otherewise
 * you may lose points when we grade your project.
*/

//mystrlen
void test1() 
{
    //Tests included in the test file 
    assert(mystrlen("01234 56789") == 11);
    assert(mystrlen("") == 0);

    //From here, these test cases I added 
    //Check the normal case 
    assert (mystrlen("cmpt 300") == 8);
    assert(mystrlen("helloworld") == 10);

    //Check if an empty string to see if it returns a length of 0
    assert(mystrlen("") == 0);
}

//mystrcpy
void test2()
{
    //Tests included in test file 
    char s1[100];
    mystrcpy (s1, "    some string  $@! 0 ");
    assert(strcmp(s1, "    some string  $@! 0 ") == 0);

    //Tests for this point on, I added 
    char* s2 = (char*)malloc(100); //allocate a buffer of 100 bytes to store strings

    //Checking the normal cases" 
    mystrcpy(s2, "Hello world");
    assert(mystrcmp(s2, "Hello world") == 0);

     if(s2 != NULL){
      free(s2);  
    }

    //Checking the case where an empty string is passed as an argument 
    char* s3 = (char*)malloc(100); 
    mystrcpy(s3, "");
    assert(mystrcmp(s3, "") == 0);

    if(s3 != NULL){
      free(s3);  
    }
}

//mystrcmp
void test3()
{
    //Tests included in test file 
    assert(mystrcmp("I love coding", "I love coding") == 0); 
    assert(mystrcmp("Abcd", "abc" ) == -1 ); 
    assert (mystrcmp("f","Fun") == 1);

    //Starting here, my tests
    //Normal case, should return 0 since they are the exact same strings
    assert(mystrcmp ("I love coding", "I love coding") == 0);
    assert(mystrcmp ("I", "I") == 0);

    //Checking the case where one or both strings are empty 
    assert(mystrcmp ("", "") == 0);
    assert(mystrcmp("HELLO", "") == 1);
    assert(mystrcmp("", "WORLD") == -1);

    //Checking the case where s1 is a shorter string than s2 
    assert(mystrcmp("A", "ab") == -1);

    //Checking the case where s2 is a shorter string than s1
    assert(mystrcmp("ab", "A") == 1);

    //Checking the case where s1, s2 are same lengths but s1 > s2, Note: ASCII value of t > T, e > E, ... ,etc 
    assert(mystrcmp("testing", "TESTING") == 1); 

    //Checking the case where s1, s2 are same lengths but s1 < s2, ASCII value of 1 < 4, 2 < 3, 3 < 6
    assert(mystrcmp("123", "456") == -1); 
}


//mystrdup
void test4()
{
    //Tests included in test file 
    char s1[100], *s2;
    s2 = mystrdup(s1);
    assert(strcmp(s1, s2) == 0); 
    assert(s1 != s2);

    //Tests from this point onwards are mine 
    //Normal test case 
    char s3[100], *s4;

    mystrcpy (s3, "I am testing my string functions!");
    assert(strcmp(s3, "I am testing my string functions!") == 0);

    s4 = mystrdup(s3);

    assert(mystrcmp(s4, s3)==0);

    if(s4 != NULL){
      free(s4);
    }

    //Check if function works on empty string
    char s5[100], *s6;
    mystrcpy (s5, "");
    assert(strcmp(s5, "") == 0);

    s6 = mystrdup(s5);

    assert(mystrcmp(s5, s6)==0);

    if(s6 != NULL){
      free(s6);
    }
}

int main( int argc, char ** argv )
{
  int testNum = 0;
  
  if ( argc < 2 ) {
    fprintf(stderr, "\n usage: %s test-num\n", argv[0]);
    return 1;
  }

  testNum = atoi(argv[1]);
  switch(testNum){
    case 1:
      test1();
      break;
    
    case 2:
      test2();
      break;
    
    case 3:
      test3();
      break;
    
    case 4:
      test4();
      break;

    default: 
      fprintf(stderr, "\n usage: %s test-num\n", argv[0]);
      return 1;
  }

  return 0;

}
